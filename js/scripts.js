let firstName = document.getElementById("first-name");
let secondName = document.getElementById("second-name");
let myMail = document.getElementById("mail");
let firstPassword = document.getElementById("first-password");
let repeatPassword = document.getElementById("repeat-password");
let enterBtn = document.getElementById("enter-btn");
let firstPasswordValue = '', sendfirstName = '', sendMail = '', sendSecondName = '', sendPassword = '';

// Check if the name is text only
function stringCheck(inputString) {
    let upperString = inputString.toUpperCase();
    let matchString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let filteredString = upperString.split('').reduce((acc, curr) => {
        if (matchString.includes(curr)) {
            acc += curr;
        }
        return acc;
    }, '');
    return upperString === filteredString;
}

// Check the password strength
function passwordCheck(userString) {
    const upperSample = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    const lowerSample = upperSample.toLowerCase();
    const symbols = " !#$%&'()*+,-./:;<=>?@[\]^_`{|}~";
    let upperPresent = "Uppercase-Letters", lowerPresent = "Lowercase-Letters", symbolPresent = "Symbols";
    userString.split('').map((curr) => {
        if (upperSample.includes(curr)) {
            upperPresent = "";
        }
        else if (lowerSample.includes(curr)) {

            lowerPresent = "";
        }
        else if (symbols.includes(curr)) {

            symbolPresent = "";
        }
    });
    return `${upperPresent} ${lowerPresent} ${symbolPresent}`
}

// Validate the email
function validateEmail(inputText) {
    var mailformat = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    return inputText.match(mailformat);
}

// First name handler
firstName.addEventListener("input", (e) => {
    let inputfirstName = firstName.value;
    if (stringCheck(inputfirstName)) {
        // console.log(inputfirstName);
        sendfirstName = inputfirstName;
        document.getElementById("fname-msg").innerText = "";
    }
    else {
        // console.log("enter text only");
        sendfirstName = '';
        document.getElementById("fname-msg").innerText = "Enter text only";
    }
});

// Second name handler
secondName.addEventListener("input", (e) => {
    let inputSecondName = secondName.value;
    if (stringCheck(inputSecondName)) {
        // console.log(inputSecondName);
        sendSecondName = inputSecondName;
        document.getElementById("sname-msg").innerText = "";
    }
    else {
        // console.log("enter text only");
        sendSecondName = '';
        document.getElementById("sname-msg").innerText = "Enter text only";
    }
});

// email handler
myMail.addEventListener("input", (e) => {
    myMailValue = myMail.value;
    // console.log(myMailValue);
    if (validateEmail(myMailValue)) {
        sendMail = myMailValue;
        document.getElementById("mail-msg").innerText = "";
    }
    else {
        document.getElementById("mail-msg").innerText = "Invalid email";
    }
});

// First password handler
firstPassword.addEventListener("input", (e) => {
    firstPasswordValue = firstPassword.value;
    let lengthMsg = "";
    if (firstPasswordValue.length < 8) {
        lengthMsg = "Password should be minimum 8 character long.";
    }
    else {
        lengthMsg = "";
    }
    let passwordCheckMsg = passwordCheck(firstPasswordValue);
    document.getElementById("fpass-msg").innerText = `${passwordCheckMsg} \n ${lengthMsg}`;

});

// Repeat password handler
repeatPassword.addEventListener("input", (e) => {
    let repeatPasswordValue = repeatPassword.value;
    if (repeatPasswordValue === firstPasswordValue) {
        // console.log("matched");
        sendPassword = firstPasswordValue;
        document.getElementById("rpass-msg").innerText = "";
    }
    else {
        sendPassword = '';
        document.getElementById("rpass-msg").innerText = "Password not matched";
    }

});


// button handler

enterBtn.addEventListener("click", (e) => {
    let checkedState = document.getElementById("terms").checked;
    // console.log(sendfirstName, sendSecondName, sendMail, sendPassword, checkedState);
    if (sendfirstName && sendSecondName && sendMail && sendPassword && checkedState) {
        document.getElementById("top-container").innerHTML='<div class="success-fill"> Account created sucessfully!<div>';
    }
    else {
        if(!sendfirstName){
            document.getElementById("fname-msg").innerText = "Please check the if first-name is entered properly!";
        }
        if(!sendSecondName){
            document.getElementById("sname-msg").innerText = "Please check the if second-name is entered properly!";
        }
        if(!sendMail){
            document.getElementById("mail-msg").innerText = "Please check the if e-mail is entered properly!";
        }
        if(!sendPassword){
            document.getElementById("fpass-msg").innerText = "Please check the entered password!";
        }
        if(!checkedState){
            document.getElementById("terms-msg").innerText = "Please accept the terms and conditions";
        }
    }
})